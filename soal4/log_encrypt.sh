#/!bin/bash

# nama file hasil enkripsi
filename=$(date "+%H":"%M %d":"%m":"%y")

# variabel untuk mendapatkan jam sekarang
hour=$(date "+%H")

# buat array untuk mendapatkan value per hurufnya
lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

# chiper1(lowercase) dan chiper2(uppercase) adalah string untuk parameter tr yang menggeser barisan alfabet
cipher1="${lowercase[$hour]}-za-${lowercase[$hour-1]%26}"
cipher2="${uppercase[$hour]}-ZA-${uppercase[$hour-1]%26}"

# pindah ke directory yang diinginkan untuk menyimpan hasil dekripsi
cd '/Users/ferzanoveri/Documents/Kuliah/Semester 4/SISOP/soal4'

# str untuk menyimpan string hasil enkripsi
echo -n "$(cat /var/log/system.log | tr 'a-z' $cipher1 | tr 'A-Z' $cipher2)" > "$filename.txt"
