#/!bin/bash

# buat file dekripsi dengan format nama file bebas
filename=$(date "+%H":"%M %d":"%m":"%y decrypt")
filename2=$(date "+%H":"%M %d":"%m":"%y")

# simpan jam sekarang di variabel hour
hour=$(date "+%H")

# buat array untuk menyimpan huruf agar bisa didapat valuenya
lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z )
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

# chiper1(lowercase) dan chiper2(uppercase) adalah string untuk parameter tr yang menggeser barisan alfabet
cipher1="${lowercase[$hour]}-za-${lowercase[$hour-1]%26}"
cipher2="${uppercase[$hour]}-ZA-${uppercase[$hour-1]%26}"

# pindah ke directory yang diinginkan untuk menyimpan hasil dekripsi
cd '/Users/ferzanoveri/Documents/Kuliah/Semester 4/SISOP/soal4'

# tuliskan hasil dekripsi ke $filename.txt
echo -n "$(cat 19:32\ 10:03:23.txt | tr $cipher1 'a-z' | tr $cipher2 'A-Z')" > "$filename.txt"
