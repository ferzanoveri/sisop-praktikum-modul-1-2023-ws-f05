# sisop-praktikum-modul-1-2023-WS-F05

| NAMA  | NRP |
| ------------- | ------------- |
| Azhar Abiyu Rasendriya Harun  | 5025211177  |
| Beauty Valen Fajri  | 5025211227 |
| Ferza Noveri  | 5025211097 |

## NOMOR 1

#### Point A
#### Menampilkan 5 Universitas dengan Ranking Tertinggi di Jepang

```shell
    sort -t, -k21rn '2023 QS World University Rankings.csv' | awk -F',' '/JP/ {print $2}' | head -5
```
> sort -t, -k21rn
- -t, berfungsi untuk menentukan separator antar field dari file yang digunakan karena file-nya csv maka separatornya koma.
- -k21 berfungsi untuk memilih kolom mana yang ingin kita sorting, -r digunakan untuk me-reverse urutan sorting, -n digunakan untuk sorting numeric atau bilangan
- " | " adalah pipe (output suatu perintah menjadi input perintah lain) .
> awk -F',' '/JP/ {print $2}'
- awk digunakan untuk memeriksa sebuah file per record/baris yang mengandung pola tertentu, ketika polanya cocok awk akan melakukan action tertentu terhadap baris tersebut.
- -F',' ini sama seperti "-t" di perintah sort, untuk menentukan separator antar field, karena filenya csv maka separatornya koma.
- '/JP/ {print $2}' berfungsi untuk memilih baris/record yang mengandung "JP" lalu kita bisa memilih field atau kolom mana yang ingin kita cetak dengan dalam hal ini kolom ke-dua.
> head -5
- Untuk menampilkan 5 urutan paling atas dari output yang sudah di-hasilkan dari command sebelumnya.

#### Point B
#### Menampilkan Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang. 

```shell
    sort -t, -k9n '2023 QS World University Rankings.csv' | awk -F',' '/JP/ {print $9}' | head -5
```
> sort -t, -k9n
- -t, berfungsi untuk menentukan separator antar field dari file yang digunakan karena file-nya csv maka separatornya koma.
- -k9 berfungsi untuk memilih kolom mana yang ingin kita sorting, -n digunakan untuk sorting numeric atau bilangan.
- " | " adalah pipe (output suatu perintah menjadi input perintah lain) .
> awk -F',' '/JP/ {print $9}'
- awk digunakan untuk memeriksa sebuah file per record/baris yang mengandung pola tertentu, ketika polanya cocok awk akan melakukan action tertentu terhadap baris tersebut.
- -F',' ini sama seperti "-t" di perintah sort, untuk menentukan separator antar field, karena filenya csv maka separatornya koma.
- '/JP/ {print $9}' berfungsi untuk memilih baris/record yang mengandung "JP" lalu kita bisa memilih field atau kolom mana yang ingin kita cetak dengan dalam hal ini kolom ke-dua.
> head -5
- Untuk menampilkan 5 urutan paling atas dari output yang sudah di-hasilkan dari command sebelumnya.

#### Point C
#### Menampilkan 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.

```shell
    sort -t, -k20rn '2023 QS World University Rankings.csv' | awk -F',' '/Japan/ {print $2}' | head -10
```
> sort -t, -k20rn
- -t, berfungsi untuk menentukan separator antar field dari file yang digunakan karena file-nya csv maka separatornya koma.
- -k20rn berfungsi untuk memilih kolom mana yang ingin kita sorting, -r digunakan untuk me-reverse urutan sorting, -n digunakan untuk sorting numeric atau bilangan.
- " | " adalah pipe (output suatu perintah menjadi input perintah lain) .
> awk -F',' '/Japan/ {print $2}'
- awk digunakan untuk memeriksa sebuah file per record/baris yang mengandung pola tertentu, ketika polanya cocok awk akan melakukan action tertentu terhadap baris tersebut.
- -F',' ini sama seperti "-t" di perintah sort, untuk menentukan separator antar field, karena filenya csv maka separatornya koma.
- '/Japan/ {print $2}' berfungsi untuk memilih baris/record yang mengandung "Japan" lalu kita bisa memilih field atau kolom mana yang ingin kita cetak dengan dalam hal ini kolom ke-dua.
> head -10
- Untuk menampilkan 10 urutan paling atas dari output yang sudah di-hasilkan dari command sebelumnya.

#### Point D
#### Menampilkan universitas dengan kata kunci "keren".

```shell
    awk -F',' '/Keren/ {print $2}' '2023 QS World University Rankings.csv'
```

> awk -F',' '/Keren/ {print $2}' 
- awk digunakan untuk memeriksa sebuah file per record/baris yang mengandung pola tertentu, ketika polanya cocok awk akan melakukan action tertentu terhadap baris tersebut.
- -F',' ini sama seperti "-t" di perintah sort, untuk menentukan separator antar field, karena filenya csv maka separatornya koma.
- '/Keren/ {print $2}' berfungsi untuk memilih baris/record yang mengandung "Keren" lalu kita bisa memilih field atau kolom mana yang ingin kita cetak dengan dalam hal ini kolom ke-dua.

## NOMOR 2

#### Point A 
#### Tampilkan cara kobeni mencoba mendownload gambar dengan beberapa ketentuan
> Untuk penyelesaian pada point A, maka langkah pertama yang harus di lakukan ialah memulai pengecekan apakah kobeni telah membuat folder untuk keinginannya, jika telah ada maka harus dicari tau folder yang telah dibuat dengan menjalankan perintah seperti code dibawah ini

```shell
catatan1()
  { 
  k=1
  while [ -d kumpulan_$k ] 
  do k=$((k+1))
  done
```

Kemudian ketika sudah melakukan pengecekan folder maka dilanjutkan dengan membuat folder atau direktori baru dengan perintah mkdir (Make Directory) untuk setiap nama kumpulan dimulai dari folder kumpulan 1 hingga folder kumpulan yang diinginkan, seperti code berikut

```shell
mkdir kumpulan_$k
```

Setelah pembuatan folder terdapat code untuk melakukan pengecekan terhadap “Waktu” saat kobeni mulai untuk mendownload gambar yang ia inginkan dengan menerapkan perintah 

```shell
WAKTU=$(date +"%H")
```

Untuk menampilkan waktu saat ini, gunakan syntax $(date +"%H") agar dapat mengetahui “Waktu” yang menunjukkan pukul berapa kobeni mendownload gambar. Kemudian gunakan perintah cd (Change Directory) untuk mengubah atau membuka direktori tertentu. Pada code yang telah ada perintah cd digunakan untuk membuka direktori folder kumpulan yang telah ada seperti contoh code berikut

```shell
cd kumpulan_$k
```

Pada soal dijelaskan bahwa pada saat waktu menunjukkan pukul 00:00 maka disetting untuk bisa mendownload 1 gambar saja dan selalu di perbarui setiap 10 jam sekali dan deklarasikan hal tersebut dengan perintah looping setiap waktunya melalui code 

```shell
if [[ $WAKTU -eq 00:00 && $(date +"%H") ]]
  then
    WAKTU=1
  fi

  for ((j=1; j<=$WAKTU; j++))
  do
```

Setelah itu lakukan perintah untuk menyimpan file dengan nama berbeda pada code

```shell
wget -O perjalanan_$j.jpg https://loremflickr.com/320/240/indonesia
```

Dalam kasus tersebut, resource yang telah di unduh akan disimpan sesuai perintah seperti halnya gambar dalam bentuk “jpg” untuk digunakan kembali.

#### Cron Job

Cron job digunakan untuk mengakses fungsi di atas melalui cronjob maka diharuskan untuk mendeklarasikan kapan saja waktu file tersebut akan dipanggil. Karena dalam soal nomor 2 dikatakan untuk setiap 10 jam sekali maka dalam waktu format cronjob adalah :

```shell
* */10 * * *
```

Kemudian deklarisakan apa yang akan system lakukan setiap 10 jam sekali dengan cara

```shell
* */10 * * * bash ~/kobeni.liburan.sh 1
```

#### Point B 
#### Kobeni mencoba untuk mengoptimalisasikan penyimpanan di komputernya dengan membuat file zip dengan beberapa ketentuan

> Kemudian penyelesaian untuk soal nomor 2 tidak sampai disitu saja, ternyata kobeni menginginkan penyimpanan di komputernya lebih di optimalisasikan dengan cara membuat zip dengan folder kumpulan yang telah ada. Hal tersebut bisa di deklarasikan dengan cara yang pertama jalankan perintah untuk code

```shell
catatan2(){
  loop=1
  while [ -d kumpulan_$loop ]
  do 
    if [ -f devil_$loop.zip ]
    then
      loop=$((loop+1))
      continue
    fi
```

Dengan tujuan melakukan perulangan apabila menemukan folder dengan nama `kumpulan_$loop` dan apabila folder tersebut pernah di zip maka lewati perintah sepertihalnya `if [ -f devil_$loop.zip ]` serta lanjutkan deklarasi untuk folder berikutnya. lakukan pengulangan tersebut di seluruh folder yang ada pada perintah `loop=$((loop+1))`

Kemudian untuk code

```shell
if [ $1 -eq 1 ]
then
    catatan1
elif [ $1 -eq 2 ]
then
    catatan2
fi
```

Dituliskan untuk memudahkan cron job dalam membantu mengoptimalisasikan perintah pada code yang telah ada pada server website dengan mudah.

## NOMOR 3

#### Point A

Membuat sistem register akun dan menyimpan username dan password pada sebuah file /users/users.txt

```shell
echo "==== REGISTER YOUR ACCOUNT HERE ===="

while true
do
  echo "Input new username: "
  read -r username
  if grep -qow "$username" users/users.txt
  then
    echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
    echo "Failed, users already exists! Enter another username"
  else
    break
  fi
done

while true
do
  echo "Input new password: "
  read -rs password
  if [[ "$password" =~ "chicken" ]] || [[ "$password" =~ "ernie" ]]
    then
    echo "Password cannot be contain to 'chicken' or 'ernie'"
  elif [[ "$password" == "$username" ]]
    then
    echo "Password cannot same as username"
  elif ! [[ ${#password} -gt 8 && "$password" =~ [A-Z] && "$password" =~ [a-z] && "$password" =~ [0-9] ]]
    then
    echo "Password must contain at least 8 character, 1 uppercase letter, 1 lowercase letter, and contains a number"
  else
    echo "Registration Successfully"
    echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> log.txt
    echo "$username+$password" >> users/users.txt
    break
  fi
done
```

>   echo "Input new username: "
>   read -r username
- echo digunakan untuk print kalimat di dalam " "
- read digunakan untuk mengambil kalimat yang dituliskan hingga user menekan tombol enter
- -r digunakan agar backslash diperlakukan seperti karakter lainnya dan bukan escape character

>   grep -qow "$username" users/users.txt
- -q untuk menghilangkan print dari grep
- -o untuk hanya cocokkan dengan pola pencarian
- -w untuk hanya cocokkan kata utuh/lengkap

>   echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
- echo digunakan untuk print kalimat di dalam " "
- "$(date)" Berfungsi untuk menunjukkan waktu saat suatu program dijalankan
- '+%y/%m/%d %H:%M:%S' Digunakan sebuah operator '+' agar kita dapat menampilkan waktu tersebut dengan format yang kita inginkan dan tidak di-outputkan secara default
- ">> log.txt" berfungsi untuk mengirim output dari command sebelum ">>" ke file "log.txt"

>   read -rs password
- -r digunakan agar backslash diperlakukan seperti karakter lainnya dan bukan escape character
- -s digunakan agar inputan menjadi tidak terlihat secara explisit.

>   if [[ "$password" =~ "chicken" ]] || [[ "$password" =~ "ernie" ]]
- digunakan untuk mengecek apakah variabel password mengandung kata "chicken" atau "ernie"

>   [[ "$password" == "$username" ]]
- digunakan untuk mengecek apakah password dan username sama

>   [[ ${#password} -gt 8 && "$password" =~ [A-Z] && "$password" =~ [a-z] && "$password" =~ [0-9] ]]
- "#" untuk menghitung panjang dari string
- untuk mengecek apakah password memenuhi syarat yang telah ditentukan soal.

> echo "$username+$password" >> users/users.txt
- ">> users/users.txt" berfungsi untuk mengirim output dari command sebelum ">>" ke file "users/users.txt"

#### Point B

Membuat sistem login akan tercatat pada log.txt
```shell
echo "==== LOGIN HERE ===="

while true
do
  echo "Username: "
  read -r username
  echo "Password: "
  read -rs password
  if grep -qow "${username}+${password}" users/users.txt
    then
    echo "Login successful as $username"
    echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt
    break
  else
    echo "Invalid username/password, please try again"
    echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
  fi
done
```
> read -rs password
- -r digunakan agar backslash diperlakukan seperti karakter lainnya dan bukan escape character
- -s digunakan agar inputan menjadi tidak terlihat secara explisit.

> if grep -qow "${username}+${password}" users/users.txt
- -q untuk menghilangkan print dari grep
- -o untuk hanya cocokkan dengan pola pencarian
- -w untuk hanya cocokkan kata utuh/lengkap

> echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt
- echo digunakan untuk print kalimat di dalam " "
- "$(date)" Berfungsi untuk menunjukkan waktu saat suatu program dijalankan
- '+%y/%m/%d %H:%M:%S' Digunakan sebuah operator '+' agar kita dapat menampilkan waktu tersebut dengan format yang kita inginkan dan tidak di-outputkan secara default
- ">> log.txt" berfungsi untuk mengirim output dari command sebelum ">>" ke file "log.txt"

> echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
- echo digunakan untuk print kalimat di dalam " "
- "$(date)" Berfungsi untuk menunjukkan waktu saat suatu program dijalankan
- '+%y/%m/%d %H:%M:%S' Digunakan sebuah operator '+' agar kita dapat menampilkan waktu tersebut dengan format yang kita inginkan dan tidak di-outputkan secara default
- ">> log.txt" berfungsi untuk mengirim output dari command sebelum ">>" ke file "log.txt"

## NOMOR 4
#### Point A
Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).

```shell
filename=$(date "+%H":"%M %d":"%m":"%y")
```
#### Penjelasan
1. `filename` adalah nama variabel yang digunakan untuk menyimpan format penamaan file enkripsi
2. `date` adalah command linux yang digunakan untuk melihat waktu saat ini.
3. `%H` digunakan untuk mendapatkan elemen jam atau hour saat ini.
4. `%M` digunakan untuk mendapat elemen menit atau minute saat ini.
5. `%d` digunakan untuk mendapat elemen hari atau day saat ini.
6. `%m` digunakan untuk mendapat elemen bulan atau month saat ini.
7. `%y` digunakan untuk mendapat elemen tahun atau year saat ini.
8. Lalu kita pisahkan elemen-elemen tersebut menggunakan ":" seperti yang diminta.

#### Output 
```shell
22:54 09:03:23
```
#### Point B

Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
- Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
- Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
- Setelah huruf z akan kembali ke huruf a

```shell
#/!bin/bash

# nama file hasil enkripsi
filename=$(date "+%H":"%M %d":"%m":"%y")

# variabel untuk mendapatkan jam sekarang
hour=$(date "+%H")

# buat array untuk mendapatkan value per hurufnya
lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

# chiper1(lowercase) dan chiper2(uppercase) adalah string untuk parameter tr yang menggeser barisan alfabet
cipher1="${lowercase[$hour]}-za-${lowercase[$hour-1]%26}"
cipher2="${uppercase[$hour]}-ZA-${uppercase[$hour-1]%26}"

# pindah ke directory yang diinginkan untuk menyimpan hasil dekripsi
cd '/home/vron/sisop/praktikum-1/sisop-praktikum-modul-1-2023-bj-a08/soal4'

# str untuk menyimpan string hasil enkripsi
echo -n "$(cat /var/log/system.log | tr 'a-z' $cipher1 | tr 'A-Z' $cipher2)" > "$filename.txt"
```
buat 2 array yang masing - masing berisi huruf - huruf lowercase dan uppercase untuk mendapatkan urutan alfabetnya sebagai berikut
```shell
lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)
```

Selanjutnya untuk menyelesaikan permasalahan chiper nya, dapat menggunakan perintah `tr` dalam linux yang digunakan untuk men-translate karakter menjadi output yang diinginkan, seperti contoh berikut ini
```shell
echo "monyet" | tr o e #mengubah o menjadi e
output : menyet
```
Sehingga kita hanya perlu men-translate semua karakter dari a-z sebanyak hour ditambahkan dengan urutan alfabet yang dapat di akses melalui array dan di modulo 26 (jumlah alfabet). Sebelum masuk pada implementasi pada problem aktualnya, berikut ini adalah contoh sederhana penggunaan tr untuk men-translate semua karakter dari a-z menjadi b-a dengan syntax sebagai berikut `tr | 'a-z' 'b-za'`, yang akan men-translate tiap huruf menjadi 1 karakter didepannya, seperti a menjadi b, b menjadi c, dan seterusnya. Atau jika kita ingin men-translate setiap karakter dengan jarak yang cukup jauh, dapat kita singkat penulisan perintah tr menjadi seperti berikut `tr | 'a-z' 'j-za-i'`, yang akan men-translate tiap huruf menjadi 10 karakter didepannya, seperti a menjadi j, b menjadi k, dan seterusnya. Sehingga kita hanya perlu menyimpan parameter output dari perintah tr tadi ke dalam sebuah variabel yang dinamakan chiper1 untuk lowercase dan chiper2 untuk uppercase sesuai jam dan urutan alfabet yang telah kita simpan sebelumnya dengan syntax seperti berikut

```shell
cipher1="${lowercase[$hour]}-za-${lowercase[$hour-1]%26}"
cipher2="${uppercase[$hour]}-ZA-${uppercase[$hour-1]%26}"
```

di mana kita dapat mengakses karakter pada array lowercase dan uppercase secara dinamis sesuai dengan variabel hour yang telah kita siapkan sebelumnya untuk mendapatkan output untuk parameter tr sesuai dengan permintaan soal. 

Jangan lupa juga untuk pindah ke directory tempat kita ingin menyimpan hasil enkripsinya. Barulah kita tuliskan hasil enkripsinya ke filename.txt yang telah kita namai sebelumnya dengan menggunakan perintah echo dan operator >.

1. `filename` adalah nama variabel yang akan digunakan untuk menyimpan format penamaan file hasil enkripsi.
2. `hour` digunakan untuk menyimpan elemen jam sekarang.
3. `lowercase` digunakan untuk menyimpan karakter - karakter lowercase.
4. `uppercase` digunakan untuk menyimpan karakter - karakter uppercase.
5. `cipher1` digunakan untuk menyimpan parameter output perintah `tr` pada karakter `lowercase`.
6. `cipher2` digunakan untuk menyimpan parameter output perintah `tr` pada karakter `uppercase`.
7. `cd '/Users/ferzanoveri/Documents/Kuliah/Semester 4/SISOP/soal4'` digunakan untuk pindah directory ke tempat hasil enkripsi disimpan.
8. tuliskan hasil enkripsinya ke `$filename.txt` menggunakan perintah `echo -n` untuk tetap menjaga white space file yang akan dienkripsi dan operator `>` untuk membuatkan file baru jika belum ada.

#### Output
```shell
Jxo  9 00:02:19 Cbowxp-JxzYllh-Mol pvpilda[143]: XPI Pbkabo Pqxqfpqfzp
Jxo  9 00:12:19 Cbowxp-JxzYllh-Mol pvpilda[143]: XPI Pbkabo Pqxqfpqfzp
Jxo  9 00:22:19 Cbowxp-JxzYllh-Mol pvpilda[143]: XPI Pbkabo Pqxqfpqfzp
Jxo  9 00:30:05 Cbowxp-JxzYllh-Mol pvpilda[143]: Zlkcfdroxqflk Klqfzb:
	XPI Jlarib "zlj.xmmib.zapzebaribo" zixfjp pbibzqba jbppxdbp.
	Qelpb jbppxdbp jxv klq xmmbxo fk pqxkaxoa pvpqbj ild cfibp lo fk qeb XPI axqxyxpb.
Jxo  9 00:30:05 Cbowxp-JxzYllh-Mol pvpilda[143]: Zlkcfdroxqflk Klqfzb:
	XPI Jlarib "zlj.xmmib.fkpqxii" zixfjp pbibzqba jbppxdbp.
	Qelpb jbppxdbp jxv klq xmmbxo fk pqxkaxoa pvpqbj ild cfibp lo fk qeb XPI axqxyxpb.
Jxo  9 00:30:05 Cbowxp-JxzYllh-Mol pvpilda[143]: Zlkcfdroxqflk Klqfzb:
	XPI Jlarib "zlj.xmmib.xrqea" pexofkd lrqmrq abpqfkxqflk "/sxo/ild/xpi" tfqe XPI Jlarib "zlj.xmmib.xpi".
	Lrqmrq mxoxjbqbop colj XPI Jlarib "zlj.xmmib.xpi" lsboofab xkv pmbzfcfba fk XPI Jlarib "zlj.xmmib.xrqea".
Jxo  9 00:30:05 Cbowxp-JxzYllh-Mol pvpilda[143]: Zlkcfdroxqflk Klqfzb:
	XPI Jlarib "zlj.xmmib.xrqea" pexofkd lrqmrq abpqfkxqflk "/sxo/ild/pvpqbj.ild" tfqe XPI Jlarib "zlj.xmmib.xpi".
	Lrqmrq mxoxjbqbop colj XPI Jlarib "zlj.xmmib.xpi" lsboofab xkv pmbzfcfba fk XPI Jlarib "zlj.xmmib.xrqea".
Jxo  9 00:30:05 Cbowxp-JxzYllh-Mol pvpilda[143]: Zlkcfdroxqflk Klqfzb:
	XPI Jlarib "zlj.xmmib.xrqea" zixfjp pbibzqba jbppxdbp.
	Qelpb jbppxdbp jxv klq xmmbxo fk pqxkaxoa pvpqbj ild cfibp lo fk qeb XPI axqxyxpb.
Jxo  9 00:30:05 Cbowxp-JxzYllh-Mol pvpilda[143]: Zlkcfdroxqflk Klqfzb:
	XPI Jlarib "zlj.xmmib.bsbkqjlkfqlo" zixfjp pbibzqba jbppxdbp.
```

#### Point C

#### Deskripsi permasalahan
Buat juga script untuk dekripsinya.

#### Solusi

Untuk menyelesaikan problem tersebut, pertama Johan perlu mendapatkan terlebih dahulu waktu saat ini (jam) menggunakan perintah date dan panggil elemen `%H`. kemudian simpan kedalam variabel hour yang nantinya akan dipakai untuk sistem chipernya. 

Langkah berikutnya adalah buat 2 array yang masing - masing berisi huruf - huruf `lowercase` dan `uppercase` untuk mendapatkan urutan alfabetnya sebagai berikut,

```shell
lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)
```

Kemudian untuk menyelesaikan problem chiper nya, kita dapat menggunakan perintah `tr` dalam linux untuk men-translate karakter menjadi output yang kita inginkan, seperti contoh berikut ini, 
```shell
echo "monyet" | tr o e #mengubah o menjadi e
output : menyet
```
Dan karena perintah soal kali ini adalah dekripsi, maka kita dapat tetap menggunakan perintah `tr` yang sama dengan hanya menukar parameter input menjadi output dan output menjadi input. Sehingga `tr` kali ini akan men-translate karakter dari `cipher1` dan `cipher2` kembali ke `a-z`
```shell
cipher1="${lowercase[$hour]}-za-${lowercase[$hour-1]%26}"
cipher2="${uppercase[$hour]}-ZA-${uppercase[$hour-1]%26}"
```

Jangan lupa juga untuk pindah ke directory tempat kita ingin menyimpan hasil dekripsinya. Barulah kita tuliskan hasil dekripsinya ke `filename.txt` yang telah kita namai sebelumnya dengan menggunakan perintah `echo` dan operator `>`

#### Penjelasan 

1. `filename` adalah nama variabel yang akan digunakan untuk menyimpan format penamaan file hasil dekripsi.
2. `hour` digunakan untuk menyimpan elemen jam sekarang.
3. `lowercase` digunakan untuk menyimpan karakter - karakter lowercase.
4. `uppercase` digunakan untuk menyimpan karakter - karakter uppercase.
5. `cipher1` digunakan untuk menyimpan parameter output perintah `tr` pada karakter `lowercase`.
6. `cipher2` digunakan untuk menyimpan parameter output perintah `tr` pada karakter `uppercase`.
7. cd `'/Users/ferzanoveri/Documents/Kuliah/Semester 4/SISOP/soal4'` digunakan untuk pindah directory ke tempat hasil dekripsi disimpan.
8. tuliskan hasil dekripsinya ke `$filename.txt` menggunakan perintah `echo -n` untuk tetap menjaga white space file yang akan didekripsi dan operator `>` untuk membuatkan file baru jika belum ada.

#### Output
```shell
Mar  9 00:02:19 Ferzas-MacBook-Pro syslogd[143]: ASL Sender Statistics
Mar  9 00:12:19 Ferzas-MacBook-Pro syslogd[143]: ASL Sender Statistics
Mar  9 00:22:19 Ferzas-MacBook-Pro syslogd[143]: ASL Sender Statistics
Mar  9 00:30:05 Ferzas-MacBook-Pro syslogd[143]: Configuration Notice:
	ASL Module "com.apple.cdscheduler" claims selected messages.
	Those messages may not appear in standard system log files or in the ASL database.
Mar  9 00:30:05 Ferzas-MacBook-Pro syslogd[143]: Configuration Notice:
	ASL Module "com.apple.install" claims selected messages.
	Those messages may not appear in standard system log files or in the ASL database.
Mar  9 00:30:05 Ferzas-MacBook-Pro syslogd[143]: Configuration Notice:
	ASL Module "com.apple.authd" sharing output destination "/var/log/asl" with ASL Module "com.apple.asl".
	Output parameters from ASL Module "com.apple.asl" override any specified in ASL Module "com.apple.authd".
Mar  9 00:30:05 Ferzas-MacBook-Pro syslogd[143]: Configuration Notice:
	ASL Module "com.apple.authd" sharing output destination "/var/log/system.log" with ASL Module "com.apple.asl".
	Output parameters from ASL Module "com.apple.asl" override any specified in ASL Module "com.apple.authd".
Mar  9 00:30:05 Ferzas-MacBook-Pro syslogd[143]: Configuration Notice:
	ASL Module "com.apple.authd" claims selected messages.
	Those messages may not appear in standard system log files or in the ASL database.
Mar  9 00:30:05 Ferzas-MacBook-Pro syslogd[143]: Configuration Notice:
	ASL Module "com.apple.eventmonitor" claims selected messages.
```

#### Point D
#### Deskripsi permasalahan
Backup file syslog setiap 2 jam untuk dikumpulkan 

#### Solusi

Untuk menyelesaikan problem tersebut, Johan perlu membuat crontab baru terlebih dahulu dengan menggunakan syntax `crontab -e`. Crontab merupakan sebuah perintah yang memberikan izin pada user untuk mengeksekusi suatu perintah tertentu atau cronjob pada waktu tertentu. Pada point ini, kita diminta untuk mem-backup file syslog setiap 2 jam untuk dikumpulkan

Hal tersebut dapat kita lakukan dengan mengatur parameter - parameter pada crontab sebagai berikut,
```shell
# m h dom mon dow command
* * * * * perintah yang akan dieksekusi
– – – – –
| | | | |
| | | | +—– day of week (0 – 7) (Sunday=0)
| | | +——- month (1 – 12)
| | +——— day of month (1 – 31)
| +———– hour (0 – 23)
+————- min (0 – 59)
```

dimana
- m - Minute (menit) - 0 to 59
- h - Hour (jam) - 0 to 23
- dom - Day of Month (tanggal) - 0 to 31
- mon - Month (bulan) - 0 to 12
- dow - Day of Week (nomor hari) - 0 to 7 (0 dan 7 adalah hari minggu)

#### Code
Karena perintah soal dilakukan setiap 2 jam sekali, maka dapat kita tulis sebagai berikut,
`0 */2 * * * /Users/ferzanoveri/Documents/Kuliah/Semester 4/SISOP/soal4/log_encrypt.sh`

#### Penjelasan 

1. `0` parameter pertama adalah menit. Karena kita butuh tepat 2 jam, maka dapat kita tulis 0.
2. `*/2` parameter kedua adalah jam, dimana penulisan seperti itu berarti bahwa perintah akan dilakukan tiap 2 jam sekali.
3. `* * *` parameter ketiga, empat, dan lima dapat kita isi * karena kita tidak butuh tanggal, bulan, atau hari tertentu untuk mengeksekusi perintah soal.
4. Dan parameter terakhir adalah command serta file path dari perintah yang akan dieksekusi yang dapat disesuaikan letak filenya.

