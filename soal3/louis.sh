#!/bin/bash
echo "==== REGISTER YOUR ACCOUNT HERE ===="

# q untuk menghilangkan print dari grep, o untuk hanya cocokkan dengan pola pencarian, dan w untuk hanya cocokkan kata utuh/lengkap, r untuk read input dan tidak peduli terhadap input selain alphanumeric
while true
do
  echo "Input new username: "
  read -r username
  if grep -qow "$username" users/users.txt
  then
    echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
    echo "Failed, users already exists! Enter another username"
  else
    break
  fi
done

while true
do
  echo "Input new password: "
  read -rs password
  if [[ "$password" =~ "chicken" ]] || [[ "$password" =~ "ernie" ]]
    then
    echo "Password cannot be contain to 'chicken' or 'ernie'"
  elif [[ "$password" == "$username" ]]
    then
    echo "Password cannot same as username"
  elif ! [[ ${#password} -gt 8 && "$password" =~ [A-Z] && "$password" =~ [a-z] && "$password" =~ [0-9] ]]
    then
    echo "Password must contain at least 8 character, 1 uppercase letter, 1 lowercase letter, and contains a number"
  else
    echo "Registration Successfully"
    echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> log.txt
    echo "$username+$password" >> users/users.txt
    break
  fi
done