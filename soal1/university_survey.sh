#!/bin/bash

# 1a
taskA(){
    sort -t, -k21rn '2023 QS World University Rankings.csv' | awk -F',' '/JP/ {print $2}' | head -5
}

# 1b
taskB(){
    sort -t, -k9n '2023 QS World University Rankings.csv' | awk -F',' '/JP/ {print $2}' | head -5
}

# 1c 
taskC(){
    sort -t, -k20 '2023 QS World University Rankings.csv' | awk -F',' '/Japan/ {print $2}' | head -10
}

# 1d
taskD(){
    awk -F',' '/Keren/ {print $2}' '2023 QS World University Rankings.csv'
}

if [ $# -eq 0 ]
  then 
  echo "Masukkan angka 1-4"
  exit
fi

if [ $1 -eq 1 ]
then
  taskA
elif [ $1 -eq 2 ]
then
  taskB
elif [ $1 -eq 3 ]
then
    taskC
elif [ $1 -eq 4 ]
then
    taskD
fi



